﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class BinarySearch : SearchStrategy
    {
        public override int Search(double[] array, double target)
        {
            int GG = array.Length - 1;
            int DG = 0;
            while (DG <= GG)
            {
                int middle = (DG + GG) / 2;
                if (target == array[middle])
                {
                    return middle;
                }
                else if (target > array[middle])
                {
                    DG = middle + 1;
                }
                else
                {
                    GG = middle - 1;
                }
            }
            return -1;
        }
    }
}
