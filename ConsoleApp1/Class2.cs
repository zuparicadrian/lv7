﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad != this.previousCPULoad)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        //public float GetCPULoad()
        //{
        //    float currentLoad = this.CPULoad;
        //    if (currentLoad != this.previousCPULoad)
        //    {
        //        if(((currentLoad/previousCPULoad)-1) > 0.1)
        //        {
        //            this.Notify();
        //        }
        //  }
        //    this.previousCPULoad = currentLoad;
        //    return currentLoad;
        //}
        public float GetAvailableRAM()
        {
            float currentLoad = this.AvailableRAM;
             if (currentLoad != this.previousRAMAvailable)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentLoad;
            return currentLoad;
        }

        //public float GetAvailableRAM()
        //{
        //    float currentLoad = this.AvailableRAM;
        //    if (currentLoad != this.previousRAMAvailable)
        //    {
        //        if (((currentLoad / previousRAMAvailable)-1) > 0.1)
        //        {
        //            this.Notify();
        //        }
        //    }
        //    this.previousRAMAvailable = currentLoad;
        //    return currentLoad;
        //}
    }
}
