﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider provider = new SystemDataProvider();

            FileLogger logg = new FileLogger("AdrianZuparic.txt");
            ConsoleLogger log = new ConsoleLogger();
            provider.Attach(log);
            provider.Attach(logg);

            while (true)
            {
                provider.GetCPULoad();
                provider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
