﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2
{
    class SequentialSearch : SearchStrategy
    {
        public override int Search(double[] array, double target)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                if (target == array[i])
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
